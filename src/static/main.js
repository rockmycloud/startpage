function createLink(childLink) {    
    let link = document.createElement("a");
    link.href = childLink.url;
    link.setAttribute('target', '_self');
    
    let linkElement = document.createElement("div");
    linkElement.classList.add("link");
    
    let linkIcon = document.createElement("img");
    linkIcon.classList.add("linkicon");
    linkIcon.src = "/api/icon?url=" + childLink.icon;
    
    let linkText = document.createElement("div");
    linkText.classList.add("linkname");

    let linkName = document.createElement("h3");
    linkName.innerText = childLink.name;
    
    linkText.appendChild(linkName);
    linkElement.appendChild(linkIcon);
    linkElement.appendChild(linkText);
    link.appendChild(linkElement);
    
    return link;
}

function createContainer(data) {

    data.forEach(d => {
        let container = document.createElement("div");
        container.classList.add("container");
        
        let containerHeader = document.createElement("div");
        containerHeader.classList.add("container_header");
        
        let containerTitle = document.createElement("h2");
        
        let containerIcon = document.createElement("i");
        containerIcon.className = d.icon;
        
        let containerName = document.createElement("span");
        containerName.innerText = d.name;
        
        containerTitle.appendChild(containerIcon);
        containerTitle.appendChild(containerName);
        containerHeader.appendChild(containerTitle);

        container.appendChild(containerHeader);

        d.links.forEach(l => {
            let link = createLink(l)
            container.appendChild(link);
        })
        
        let startpage = document.querySelector(".container_main");
        startpage.appendChild(container);
    });
}

document.addEventListener('DOMContentLoaded', (e) => {

    fetch("/api/container")
    .then(response => response.json())
    .then(data => createContainer(data.container))

});
