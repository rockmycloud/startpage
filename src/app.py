from flask import Flask, render_template, request, stream_with_context, Response
import json
import requests


app = Flask(__name__)


@app.route("/")
def index():
    return render_template("index.html")


@app.route("/api/icon")
def icon():
    url = request.args.get('url')

    icon_url = f"https://www.google.com/s2/favicons?domain={url}"

    icon = requests.get(icon_url, stream=True)

    response = Response(stream_with_context(icon.iter_content(chunk_size=2048)))
    response.headers['Content-Type'] = icon.headers["content-type"]
    response.headers['Content-Disposition'] = 'attachment; filename="icon.ico"'

    return response


@app.route("/api/container")
def data():
    f = open('./data.json')
    data = json.load(f)
    f.close()
    return data