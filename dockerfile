FROM python:3.10-bullseye

COPY ./src /src
WORKDIR /src

RUN pip3 install -r requirements.txt

CMD ["python", "-m", "flask", "run", "--host=0.0.0.0"]

EXPOSE 5000